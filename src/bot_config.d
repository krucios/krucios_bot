module bot_config;

import std.json;
import std.conv;
import std.string;

unittest {
    BotConfig cfg;
    assert(cfg.api_token == "");
    assert(cfg.polling_timeout_sec == 600);

    string s = `{
                    "api_token"             : "123456:ABC-DEF1234ghIkl-zyx57W2v1u123ew11",
                    "polling_timeout_sec"   : 3600,
                }`;
    cfg.fromJSON(parseJSON(s));
    assert(cfg.api_token == "123456:ABC-DEF1234ghIkl-zyx57W2v1u123ew11");
    assert(cfg.polling_timeout_sec == 3600);

    assert(cfg.toString == "BotConfig\napi_token: 123456:ABC-DEF1234ghIkl-zyx57W2v1u123ew11\npolling_timeout_sec: 3600");
}

struct BotConfig {
    string api_token;
    ulong polling_timeout_sec = 600;

    void fromJSON(JSONValue json) {
        api_token = json["api_token"].str;
        polling_timeout_sec = ("polling_timeout_sec" in json.object) ? json["polling_timeout_sec"].integer : polling_timeout_sec;
    }

    string toString() {
        string res;
        res ~= "BotConfig";
        res ~= "\napi_token: " ~ api_token;
        res ~= "\npolling_timeout_sec: " ~ to!string(polling_timeout_sec);
        return res;
    }
}