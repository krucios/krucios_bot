module bot;

import std.net.curl;
import std.experimental.logger;
import std.json;
import std.conv;
import std.string;
import std.file;

import core.thread;

import update;
import user;
import message;
import reply_keyboard_markup;
import reply_keyboard_remove;

import bot_config;

immutable LAST_UPD_ID_FILE = ".last_upd_id";

alias UpdHandler = void delegate(Bot, ref Update);

enum Markdown_t {
    NONE,
    MARKDOWN,
    HTML
};

class Bot {
    BotConfig   m_cfg;
    string      m_url;
    ulong       m_cur_update;
    bool        m_bot_en = false;
    HTTP        m_http;
    Thread      m_composed;
    Markdown_t  m_markdown_mode;

    UpdHandler  m_msg_handler                   = null;
    UpdHandler  m_edited_msg_handler            = null;
    UpdHandler  m_channel_post_handler          = null;
    UpdHandler  m_edited_channel_post_handler   = null;

    JSONValue HTTPget(string method_name) {
        JSONValue resp;
        string str;

        m_http = HTTP(this.m_url ~ method_name);
        m_http.method = HTTP.Method.get;
        m_http.onReceive = (ubyte[] data) {
            str = cast(string)data;
            return data.length;
        };
        m_http.perform();

        resp = parseJSON(str);
        if (resp["ok"].type != JSON_TYPE.TRUE) {
            error("Error on sending message\nRES:\n" ~ resp.toPrettyString);
        }
        return resp;
    }

    JSONValue HTTPpostJSON(string method_name, ref JSONValue json) {
        JSONValue resp;
        string str;

        m_http = HTTP(this.m_url ~ method_name);
        m_http.method = HTTP.Method.post;
        m_http.setPostData(json.toString, "application/json");
        m_http.onReceive = (ubyte[] data) {
            str = cast(string)data;
            return data.length;
        };
        m_http.perform();

        resp = parseJSON(str);
        if (resp["ok"].type != JSON_TYPE.TRUE) {
            error("Error on sending message\nREQ:\n" ~ json.toPrettyString ~ "\nRES:\n" ~ resp.toPrettyString);
        }
        return resp;
    }

    void sendMessage(ref JSONValue json) {
        final switch (m_markdown_mode) {
            case Markdown_t.MARKDOWN : {
                json["parse_mode"] = JSONValue("Markdown");
                break;
            }
            case Markdown_t.HTML : {
                json["parse_mode"] = JSONValue("HTML");
                break;
            }
            case Markdown_t.NONE : {
                break;
            }
        }
        HTTPpostJSON("sendMessage", json);
    }

    JSONValue getUpdates(long offset, int limit = 1, string[] allowed_updates = []) {
        JSONValue json;

        json["offset"] = JSONValue(offset);
        json["limit"] = JSONValue(limit);
        json["timeout"] = JSONValue(m_cfg.polling_timeout_sec);
        json["allowed_updates"] = JSONValue(allowed_updates);

        return HTTPpostJSON("getUpdates", json);
    }

    void getUpdRoutine() {
        long marker = 0;

        while (m_bot_en) {
            auto json = this.getUpdates(this.cur_update + 1, 1);

            if (json["ok"].type == JSON_TYPE.TRUE) {
                if (json["result"].array.length) {
                    Update upd;
                    upd.fromJSON(json["result"].array[0]);

                    if (cur_update <= upd.id) {
                        cur_update = upd.id;
                    }

                    final switch (upd.type) {
                        case Update_t.NONE: {
                            error("Updates of this type currently is not supported.\n" ~ upd.toString);
                            break;
                        }
                        case Update_t.MESSAGE: {
                            if (m_msg_handler !is null) {
                                m_msg_handler(this, upd);
                            }
                            break;
                        }
                        case Update_t.EDITED_MESSAGE: {
                            if (m_edited_msg_handler !is null) {
                                m_edited_msg_handler(this, upd);
                            }
                            break;
                        }
                        case Update_t.CHANNEL_POST: {
                            if (m_channel_post_handler !is null) {
                                m_channel_post_handler(this, upd);
                            }
                            break;
                        }
                        case Update_t.EDITED_CHANNEL_POST: {
                            if (m_edited_channel_post_handler !is null) {
                                m_edited_channel_post_handler(this, upd);
                            }
                            break;
                        }
                    }
                }
            } else {
                error("Something wrong happened\n" ~ json.toPrettyString);
            }

            log("[\t" ~ to!string(marker++) ~ "] <<<");
        }
    }

    void storeCurUpdID() {
        write(LAST_UPD_ID_FILE, to!string(this.cur_update));
    }

public:
    this(const BotConfig cfg) {
        this.m_cfg = cfg;
        this.m_url = "https://api.telegram.org/bot" ~ this.m_cfg.api_token ~ "/";

        if (exists(LAST_UPD_ID_FILE)) {
            this.cur_update = to!ulong(readText(LAST_UPD_ID_FILE));
        } else {
            this.cur_update = 1;
        }
    }

    JSONValue getMe() {
        return HTTPget("getMe");
    }

    void sendMessage(long u_id, string text) {
        JSONValue json;

        json["chat_id"] = JSONValue(u_id);
        json["text"] = JSONValue(text);

        sendMessage(json);
    }

    void sendMessage(long u_id, string text, ReplyKeyboardMarkup keyboard) {
        JSONValue json;

        json["chat_id"] = JSONValue(u_id);
        json["text"] = JSONValue(text);
        json["reply_markup"] = keyboard.toJSON();

        sendMessage(json);
    }

    void sendMessage(long u_id, string text, ReplyKeyboardRemove keyboard) {
        JSONValue json;

        json["chat_id"] = JSONValue(u_id);
        json["text"] = JSONValue(text);
        json["reply_markup"] = keyboard.toJSON();

        sendMessage(json);
    }

    void sendMessage(string name, string text) {
        JSONValue json;

        json["chat_id"] = JSONValue("@" ~ name);
        json["text"] = JSONValue(text);

        sendMessage(json);
    }

    void start() {
        m_bot_en = true;
        m_composed = new Thread(&this.getUpdRoutine);
        m_composed.start();
    }

    void stop() {
        storeCurUpdID();
        m_bot_en = false;
    }

    @property ulong cur_update() { return m_cur_update; }
    @property ulong cur_update(ulong val) { return m_cur_update = val; }

    @property UpdHandler message_handler() { return m_msg_handler; }
    @property UpdHandler message_handler(UpdHandler dg) { return m_msg_handler = dg; }

    @property UpdHandler edited_message_handler() { return m_edited_msg_handler; }
    @property UpdHandler edited_message_handler(UpdHandler dg) { return m_edited_msg_handler = dg; }

    @property UpdHandler channel_post_handler() { return m_channel_post_handler; }
    @property UpdHandler channel_post_handler(UpdHandler dg) { return m_channel_post_handler = dg; }

    @property UpdHandler edited_channel_post_handler() { return m_edited_channel_post_handler; }
    @property UpdHandler edited_channel_post_handler(UpdHandler dg) { return m_edited_channel_post_handler = dg; }

    @property Markdown_t markdown_mode() { return m_markdown_mode; }
    @property Markdown_t markdown_mode(string md) {
        switch (md) {
            case "Markdown" : {
                m_markdown_mode = Markdown_t.MARKDOWN;
                break;
            }
            case "HTML" : {
                m_markdown_mode = Markdown_t.HTML;
                break;
            }
            default : {
                m_markdown_mode = Markdown_t.NONE;
                break;
            }
        }
        return m_markdown_mode;
    }
}