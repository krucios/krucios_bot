module keyboard_button;

import std.json;
import std.string;

struct KeyboardButton {
    string text;
    bool request_contact;
    bool request_location;

    void fromJSON(ref JSONValue json) {
        text = json["text"].str;
        request_contact = ("request_contact" in json.object) ? (json["request_contact"].type == JSON_TYPE.TRUE) : false;
        request_location = ("request_location" in json.object) ? (json["request_location"].type == JSON_TYPE.TRUE) : false;
    }

    JSONValue toJSON() {
        JSONValue res;

        res["text"] = JSONValue(text);
        res["request_contact"] = JSONValue(request_contact);
        res["request_location"] = JSONValue(request_location);

        return res;
    }

    string toString() {
        string res;

        res ~= "KeyboardButton";
        res ~= " text: " ~ text;
        res ~= " request_contact: " ~ request_contact;
        res ~= " request_location: " ~ request_location;

        return res;
    }
}