module reply_keyboard_remove;

import std.json;
import std.string;

struct ReplyKeyboardRemove {
    immutable bool remove_keyboard = true;
    bool selective;

    void fromJSON(ref JSONValue json) {
        selective = ("selective" in json.object) ? (json["selective"].type == JSON_TYPE.TRUE) : false;
    }

    JSONValue toJSON() {
        JSONValue res;

        res["remove_keyboard"] = JSONValue(remove_keyboard);
        res["selective"] = JSONValue(selective);

        return res;
    }

    string toString() {
        string res;

        res ~= "ReplyKeyboardRemove";
        res ~= " selective: " ~ selective;

        return res;
    }
}