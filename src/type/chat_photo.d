module chat_photo;

import std.json;
import std.string;

struct ChatPhoto {
    string small_file_id;
    string big_file_id;

    void fromJSON(ref JSONValue json) {
        small_file_id = json["small_file_id"].str;
        big_file_id   = json["big_file_id"].str;
    }

    string toString() {
        string res;

        res ~= "ChatPhoto";
        res ~= " small: " ~ small_file_id;
        res ~= " big: " ~ big_file_id;

        return res;
    }
}