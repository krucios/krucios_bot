module reply_keyboard_markup;

import std.json;
import std.string;
import keyboard_button;

struct ReplyKeyboardMarkup {
    KeyboardButton[][] keyboard;
    bool resize_keyboard;
    bool one_time_keyboard;
    bool selective;

    void fromJSON(ref JSONValue json) {
        keyboard.length = json["keyboard"].array.length;
        for (int i = 0; i < keyboard.length; i++) {
            keyboard[i].length = json["keyboard"].array[i].array.length;
            for (int j = 0; j < keyboard[i].length; j++) {
                keyboard[i][j].fromJSON(json["keyboard"].array[i].array[j]);
            }
        }

        resize_keyboard     = ("resize_keyboard" in json.object)    ? (json["resize_keyboard"].type == JSON_TYPE.TRUE)      : false;
        one_time_keyboard   = ("one_time_keyboard" in json.object)  ? (json["one_time_keyboard"].type == JSON_TYPE.TRUE)    : false;
        selective           = ("selective" in json.object)          ? (json["selective"].type == JSON_TYPE.TRUE)            : false;
    }

    JSONValue toJSON() {
        JSONValue res;
        JSONValue[][] json_keyboard;

        json_keyboard.length = keyboard.length;
        foreach (i, row; keyboard) {
            json_keyboard[i].length = keyboard[i].length;
            foreach (j, e; row) {
                json_keyboard[i][j] = e.toJSON();
            }
        }

        res["keyboard"]          = JSONValue(json_keyboard);
        res["resize_keyboard"]   = JSONValue(resize_keyboard);
        res["one_time_keyboard"] = JSONValue(one_time_keyboard);
        res["selective"]         = JSONValue(selective);

        return res;
    }

    string toString() {
        string res;

        res ~= "ReplyKeyboardMarkup.";
        res ~= " resize_keyboard: " ~ resize_keyboard;
        res ~= " one_time_keyboard: " ~ one_time_keyboard;
        res ~= " selective: " ~ selective;

        return res;
    }
}