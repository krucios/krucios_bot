module user;

import std.json;
import std.conv;
import std.string;

struct User {
    ulong   id;             // Unique identifier for this user or bot
    bool    is_bot;         // True, if this user is a bot
    string  first_name;     // User‘s or bot’s first name
    string  last_name;      // Optional. User‘s or bot’s last name
    string  username;       // Optional. User‘s or bot’s username
    string  language_code;  // Optional. IETF language tag of the user's language

    void fromJSON(ref JSONValue json) {
        id          =  json["id"].integer;
        is_bot      = (json["is_bot"].type == JSON_TYPE.TRUE);
        first_name  =  json["first_name"].str;

        last_name       = ("last_name" in json.object)      ? json["last_name"].str     : "";
        username        = ("username" in json.object)       ? json["username"].str      : "";
        language_code   = ("language_code" in json.object)  ? json["language_code"].str : "";
    }

    string toString() {
        string res;

        res ~= is_bot ? "Bot" : "User";
        res ~= " (" ~ to!string(id) ~ ") ";
        res ~= first_name;
        res ~= (last_name != "") ? (" " ~ last_name) : "";
        res ~= (username != "") ? (" (uname: " ~ username ~ ")") : "";
        res ~= (language_code != "") ? (" [" ~ language_code ~ "]") : "";

        return res;
    }
}