module update;

import std.json;
import std.conv;
import std.string;

import message;

enum Update_t {
    NONE,
    MESSAGE,
    EDITED_MESSAGE,
    CHANNEL_POST,
    EDITED_CHANNEL_POST
}

struct Update {
    ulong   id;
    Message message;
    Message edited_message;
    Message channel_post;
    Message edited_channel_post;

    Update_t type = Update_t.NONE;

    void fromJSON(ref JSONValue json) {
        id = json["update_id"].integer;

        if ("message" in json.object) {
            message.fromJSON(json["message"]);
            type = Update_t.MESSAGE;
        } else
        if ("edited_message" in json.object) {
            edited_message.fromJSON(json["edited_message"]);
            type = Update_t.EDITED_MESSAGE;
        } else
        if ("channel_post" in json.object) {
            channel_post.fromJSON(json["channel_post"]);
            type = Update_t.CHANNEL_POST;
        } else
        if ("edited_channel_post" in json.object) {
            edited_channel_post.fromJSON(json["edited_channel_post"]);
            type = Update_t.EDITED_CHANNEL_POST;
        }
    }

    string toString() {
        string res;

        res ~= "Update (" ~ to!string(id) ~ ")";
        res ~= " type: ";
        final switch (type) {
            case Update_t.NONE: {
                res ~= "none";
                break;
            }
            case Update_t.MESSAGE: {
                res ~= "message";
                break;
            }
            case Update_t.EDITED_MESSAGE: {
                res ~= "edited_message";
                break;
            }
            case Update_t.CHANNEL_POST: {
                res ~= "channel_post";
                break;
            }
            case Update_t.EDITED_CHANNEL_POST: {
                res ~= "edited_channel_post";
                break;
            }
        }

        return res;
    }
}