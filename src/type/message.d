module message;

import std.json;
import std.conv;
import std.string;

import user;
import chat;

struct Message {
    ulong   id;            // Unique message identifier inside this chat
    User    from;          // Optional. Sender, empty for messages sent to channels
    ulong   date;          // Date the message was sent in Unix time
    Chat    chat;          // Conversation the message belongs to
    string  text;          // Optional. For text messages, the actual UTF-8 text of the message, 0-4096 characters.

    void fromJSON(ref JSONValue json) {
        id      = json["message_id"].integer;
        if ("from" in json.object) { from.fromJSON(json["from"]); }
        date    = json["date"].integer;
        text    = ("text" in json.object) ? json["text"].str : "";
        chat.fromJSON(json["chat"]);
    }

    string toString() {
        string res;

        res ~= "Message";
        res ~= " at " ~ to!string(date);
        res ~= (from.id != 0) ? (" from: " ~ from.toString) : "";
        res ~= (text != "") ? ("\ntext: " ~ text) : "";

        return res;
    }
}