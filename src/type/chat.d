module chat;

import std.json;
import std.conv;
import std.string;

import message;
import chat_photo;

struct Chat {
    ulong       id;
    string      type;
    string      title;
    string      username;
    string      first_name;
    string      last_name;
    bool        all_members_are_administrators;
    ChatPhoto   photo;
    string      description;
    string      invite_link;
    // TODO: solve recursion
    // Message     pinned_message;

    void fromJSON(ref JSONValue json) {
        id          = json["id"].integer;
        type        = json["type"].str;
        title       = ("title" in json.object) ? json["title"].str : "";
        username    = ("username" in json.object) ? json["username"].str : "";
        first_name  = ("first_name" in json.object) ? json["first_name"].str : "";
        last_name   = ("last_name" in json.object) ? json["last_name"].str : "";
        all_members_are_administrators = ("all_members_are_administrators" in json.object) ? (json["all_members_are_administrators"].type == JSON_TYPE.TRUE) : false;
        if ("photo" in json.object) { photo.fromJSON(json["photo"]); }
        description = ("description" in json.object) ? json["description"].str : "";
        invite_link = ("invite_link" in json.object) ? json["invite_link"].str : "";

        debug {
            assert((type == "private")
                || (type == "group")
                || (type == "supergroup")
                || (type == "channel"));
        }

/*
        if ("pinned_message" in json.object) {
            pinned_message.fromJSON(json["pinned_message"]);
        }
*/
    }

    string toString() {
        string res;

        res ~= "Chat (" ~ to!string(id) ~ ")";
        res ~= " [" ~ type ~ "]";
        res ~= (title != "") ? (" title: " ~ title) : "";

        return res;
    }
}