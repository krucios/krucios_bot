module questions;

import std.json;
import std.conv;
import std.string;

struct Question {
    string   text;
    string[] variants;
    long     answer;

    void fromJSON(ref JSONValue json) {
        text = json["text"].str;
        variants.length = json["variants"].array.length;
        for (int i = 0; i < variants.length; i++) {
            variants[i] = strip(chompPrefix(json["variants"].array[i].str, `\* `));
            if (startsWith(variants[i], "Правильный ответ:")) {
                variants[i] = strip(chompPrefix(variants[i], "Правильный ответ:"));
                answer = i + 1;
            }
        }
    }

    string toString() {
        string res;

        res ~= text ~ "\n";
        foreach (i, e; variants) {
            res ~= to!string(i + 1) ~ " : " ~ e ~ "\n";
        }

        return res;
    }
}

struct Questions {
    Question[] questions;

    void fromJSON(JSONValue json) {
        questions.length = json["questions"].array.length;
        for (int i = 0; i < questions.length; i++) {
            questions[i].fromJSON(json["questions"].array[i]);
        }
    }

    string toString() {
        string res;

        res ~= "Questions [\n";
        foreach (e; questions) {
            res ~= e.toString ~ "\n";
        }
        res ~= "]\n";

        return res;
    }
}