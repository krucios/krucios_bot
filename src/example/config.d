module config;

import std.json;
import std.conv;
import std.string;

import bot_config;

unittest {
    Config cfg;
    assert(cfg.bot_cfg.api_token == "");
    assert(cfg.bot_cfg.polling_timeout_sec == 600);
    assert(cfg.moderators.length == 0);
    assert(cfg.admins.length == 0);

    string s = `{
                    "bot": {
                        "api_token"             : "123456:ABC-DEF1234ghIkl-zyx57W2v1u123ew11",
                        "polling_timeout_sec"   : 3600,
                    },
                    "moderators"    : [1],
                    "admins"        : [2],
                }`;
    cfg.fromJSON(parseJSON(s));
    assert(cfg.bot_cfg.api_token == "123456:ABC-DEF1234ghIkl-zyx57W2v1u123ew11");
    assert(cfg.bot_cfg.polling_timeout_sec == 3600);
    assert(cfg.moderators.length == 1);
    assert(cfg.moderators[0] == 1);
    assert(cfg.admins[0] == 2);

    assert(cfg.toString == "Config"
                          ~ "admins: ["
                          ~ "    2,"
                          ~ " ]"
                          ~ "moderators: ["
                          ~ "    1,"
                          ~ "]"
                          ~ "BotConfig"
                          ~ "api_token: 123456:ABC-DEF1234ghIkl-zyx57W2v1u123ew11"
                          ~ "polling_timeout_sec: 3600");
}

struct Config {
    BotConfig bot_cfg;
    long[] moderators;
    long[] admins;

    void fromJSON(JSONValue json) {
        bot_cfg.fromJSON(json["bot"]);

        moderators.length = json["moderators"].array.length;
        for (int i = 0; i < moderators.length; i++) {
            moderators[i] = json["moderators"].array[i].integer;
        }

        admins.length = json["admins"].array.length;
        for (int i = 0; i < admins.length; i++) {
            admins[i] = json["admins"].array[i].integer;
        }
    }

    string toString() {
        string res;

        res ~= "Config\nadmins: [\n";
        foreach (e; admins) {
            res ~= "    " ~ to!string(e) ~ ",\n";
        }
        res ~= "]\nmoderators: [\n";
        foreach (e; moderators) {
            res ~= "    " ~ to!string(e) ~ ",\n";
        }
        res ~= "]\n";
        res ~= bot_cfg.toString;

        return res;
    }
}