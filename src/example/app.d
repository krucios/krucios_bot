import std.json;
import std.conv;
import std.string;
import std.getopt;
import std.file;

import std.experimental.logger;

import update;
import user;
import message;
import bot;
import quizlet_router;
import keyboard_button;
import reply_keyboard_remove;
import reply_keyboard_markup;

import config;
import questions;

void main(string[] argv) {
    string cfg_file_name;
    string questions_file_name;
    Config cfg;

    auto opt_help_info = getopt(
        argv,
        "api_token|t", "API_TOKEN of your Telegram bot", &cfg.bot_cfg.api_token,
        std.getopt.config.required,
        "config|c", "Config file", &cfg_file_name,
        std.getopt.config.required,
        "questions|q", "Questions file", &questions_file_name,
    );

    if (opt_help_info.helpWanted) {
        defaultGetoptPrinter("Command line options description.",
            opt_help_info.options);
        return;
    }

    auto cfg_str = to!string(read(cfg_file_name));
    cfg.fromJSON(parseJSON(cfg_str));

    info(cfg.toString);

    Bot bot = new Bot(cfg.bot_cfg);
    QuizletRouter router = new QuizletRouter(cfg);

    ReplyKeyboardMarkup keyboard = {
        keyboard: [],
        resize_keyboard: true,
        one_time_keyboard: false,
        selective: false
    };
    ReplyKeyboardRemove rem_keyboard;

    Questions q;
    auto questions_str = to!string(read(questions_file_name));
    q.fromJSON(parseJSON(questions_str));
    router.questions = q;
    log("Questions were successfully loaded from JSON file. Questions count: " ~ to!string(q.questions.length));

    bot.markdown_mode = "Markdown";
    bot.message_handler = delegate void(Bot bot, ref Update upd) {
        string resp;

        log(upd.message.toString);

        if (upd.message.text.length) {
            router.switch_context(upd.message.from.id);

            if (upd.message.text[0] == '/') { // Command
                if (upd.message.text[1 .. $] != "") {
                    resp = router.route(upd.message.text[1 .. $]);
                } else {
                    resp = "Command name is missed. Please try /help to see available commands.";
                }
            } else if (router.is_test) { // Answer to question
                if (upd.message.text.isNumeric) {
                    resp = router.route("ans " ~ upd.message.text.strip);
                } else {
                    resp = "Answer should be a number";
                }
            } else {
                resp = "Please, start the quiz by sending /test.\nFor more information send /help.";
            }

            if (router.is_test) {
                size_t n = router.variants_cnt;

                keyboard.keyboard.length = 1;
                keyboard.keyboard[0].length = n;

                for (int i = 0; i < n; i++) {
                    keyboard.keyboard[0][i] = *(new KeyboardButton);
                    keyboard.keyboard[0][i].text = to!string(i + 1);
                }

                bot.sendMessage(upd.message.from.id, resp, keyboard);
            } else {
                bot.sendMessage(upd.message.from.id, resp, rem_keyboard);
            }

            if (startsWith(resp, "The test")) {
                foreach (e; cfg.moderators) {
                    bot.sendMessage(e, upd.message.from.toString ~ "\n" ~ resp);
                }
            }

            if (startsWith(resp, "Shutdown")) {
                bot.stop();
            }
        } else {
            bot.sendMessage(upd.message.from.id, "👍 axaxaxax");
        }
    };

    bot.channel_post_handler = delegate void(Bot bot, ref Update upd) {
        log("Channel [", upd.channel_post.chat.id, "] : ", upd.channel_post.text);
        bot.sendMessage(upd.channel_post.chat.id , "Repost: '" ~ capitalize(upd.channel_post.text) ~ "'");
    };

    auto j = bot.getMe();
    if (j["ok"].type == JSON_TYPE.TRUE) {
        User me;
        me.fromJSON(j["result"]);
        log(me.toString);
    } else {
        error("Something wrong happened\n" ~ j.toPrettyString);
    }

    log("=================================================");

    bot.start();
}
