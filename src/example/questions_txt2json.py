# coding: utf-8

import re
code_re = re.compile(r"</?code>")
fout = open("questions_temp.txt", "w", encoding="utf-8")
with open("questions.txt", "r", encoding="utf-8") as f:
    for line in f:
        if ((line != "\n") and (line != " \n")):
            line = code_re.sub("`", line)
            line = line.replace("*", "\*")
            line = line.replace("_", "\_")
            line = line.replace("[", "\[")
            fout.write(line)

fout.close()

is_next_q = False
question = ""
questions = []
with open("questions_temp.txt", "r", encoding="utf-8") as f:
    for line in f:
        if (line != "\n"):
            if ("==== Ответы ====" in line):
                questions.append(question)
                question = ""
                is_next_q = False
            if (is_next_q):
                question += line
            if ("Вопрос:" in line):
                is_next_q = True

for q in questions:
    print(q)

is_next_variants = False
variants = []
answers = []
with open("questions_temp.txt", "r", encoding="utf-8") as f:
    for line in f:
        if ((line != "\n") and (line != " \n")):
            if ("Вопрос:" in line):
                is_next_variants = False
            if (is_next_variants):
                variants.append(line)
            if ("=== Ответы ===" in line):
                if (len(variants)):
                    answers.append(variants)
                variants = []
                is_next_variants = True
if (is_next_variants):
    answers.append(variants)

for ans in answers:
    print("================================")
    for v in ans:
        print(v)

print("============ Result ============");
print("Question count: ", len(questions));
print("Answers count: ", len(answers));

import json

fout = open("questions.json", "w", encoding="utf-8")
obj = {}
obj['questions'] = []
for i in range(len(questions)):
    el = {'text' : questions[i], 'variants' : answers[i]}
    obj['questions'].append(el)

import os;

# os.remove("questions_temp.txt")
fout.write(json.JSONEncoder().encode(obj))

