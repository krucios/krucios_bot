module jenkins_router;

import std.net.curl;
import std.json;
import std.conv;
import std.string;
import std.array;
import std.file;

import std.stdio : writeln;

import router;

class JenkinsRouter : Router {
    string regress_fname = "jenkins_paths.json";
    string cov_fname = "jenkins_cov_paths.json";

    string start_handler(const string[] argv) {
        return "Nice to see you!";
    }

    string help_handler(const string[] argv) {
        return
`Available commands:
/start - just for start
/help - to see this message
/regress <proj_name> - to get last regress results for specified project
/regress add <proj_name> <path_to_file> - add project`;
    }

    string regress_handler(const string[] argv) {
        string resp;
        string req_url;
        string[] lines;
        string paths;
        JSONValue json;

        if (!exists(regress_fname)) {
            write(regress_fname, "{}");
        }

        paths = readText(regress_fname);
        json = parseJSON(paths);

        if (argv.length > 1) {
            switch (argv[1]) {
                case "add": {
                    if (argv.length >= 4) {
                        json[argv[2]] = JSONValue(argv[3]);
                        write(regress_fname, json.toString);

                        resp = argv[2] ~ " was set as: " ~ argv[3];
                    } else {
                        resp = "Error: argument count mismatch";
                    }
                    break;
                }
                default: {
                    if (argv[1] in json.object) {
                        req_url = json[argv[1]].str;

                        http = HTTP(req_url);
                        http.method = HTTP.Method.get;
                        http.onReceive = (ubyte[] data) {
                            resp = cast(string)data;
                            return data.length;
                        };
                        http.perform();

                        lines = resp.split("\n");
                        resp  = lines[$ - 3].replace("\t", "\n");
                    } else {
                        resp = "Error: url [" ~ argv[1] ~ "] doesn't exist";
                    }
                }
            }
        } else {
            resp = "Error: please specify a project name\nAvailable projects: ";
            resp ~= json.object.keys.join("\n");
        }

        return resp;
    }

    string cov_handler(const string[] argv) {
        string resp;
        string req_url;
        string[] lines;
        string paths;
        JSONValue json;

        if (!exists(cov_fname)) {
            write(cov_fname, "{}");
        }

        paths = readText(cov_fname);
        json = parseJSON(paths);

        if (argv.length > 1) {
            switch (argv[1]) {
                case "add": {
                    if (argv.length >= 4) {
                        json[argv[2]] = JSONValue(argv[3]);
                        write(cov_fname, json.toString);

                        resp = argv[2] ~ " was set as: " ~ argv[3];
                    } else {
                        resp = "Error: argument count mismatch";
                    }
                    break;
                }
                default: {
                    if (argv[1] in json.object) {
                        req_url = json[argv[1]].str;

                        http = HTTP(req_url);
                        http.method = HTTP.Method.get;
                        http.onReceive = (ubyte[] data) {
                            resp = cast(string)data;
                            return data.length;
                        };
                        http.perform();

                        lines = resp.split("\n");
                        lines = lines[$ - 23 .. $ - 20];

                        auto titles = lines[1].split();
                        auto scores = lines[2].split();

                        resp = lines[0];
                        for (int i = 0; i < titles.length; i++) {
                            resp ~= "\n" ~ titles[i] ~ ": " ~ scores[i];
                        }
                    } else {
                        resp = "Error: url [" ~ argv[1] ~ "] doesn't exist";
                    }
                }
            }
        } else {
            resp = "Error: please specify a project name\nAvailable projects: ";
            resp ~= json.object.keys.join("\n");
        }

        return resp;
    }

public:
    this() {}

    override string route(const string req) {
        string[] words = req.split;
        string res;

        switch (words[0]) {
            case "start": {
                res = start_handler(words);
                break;
            }
            case "help": {
                res = help_handler(words);
                break;
            }
            case "regress": {
                res = regress_handler(words);
                break;
            }
            case "cov": {
                res = cov_handler(words);
                break;
            }
            default: {
                res = "Unknown command. Please try /help";
                break;
            }
        }

        return res;
    }
}