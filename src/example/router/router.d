module router;

import std.net.curl;
import std.string;
import std.exception : Exception;

alias handler_t = string delegate(string[]);

class Router {
    HTTP http;

    handler_t[string] m_route_table;

public:
    this() {}

    string route(const string req) {
        string[] argv = req.split;
        string resp;

        if (argv[0] in m_route_table) {
            resp = m_route_table[argv[0]](argv);
        } else {
            resp = "Unknown command. Please try /help to see available commands";
        }

        return resp;
    }

    void on(string CMD)(handler_t handler) {
        m_route_table[CMD] = handler;
    }
}