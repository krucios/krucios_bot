module quizlet_router;

import std.net.curl;
import std.json;
import std.conv;
import std.string;
import std.array : split, join;
import std.file;
import std.variant;
import std.stdio : writeln;
import std.random;
import std.exception;
import std.algorithm.searching : canFind;

import router;
import questions;
import config;

immutable VAR_DELIM = ";";
immutable HELP_MESSAGE =
`List of available commands.
/start - just to get few kind words.
/test - to start quiz. After this command you will receive first question. Custom keyboard will appear.
/reset - to reset bot to initial state. It will close custom keyboard.

After sending "/test" just press buttons with correct answer and after some questions you will get results.
`;

struct UserContext {
    bool m_is_test_active = false;
    uint m_correct_ans_cnt = 0;
    size_t m_total_q_cnt = 0;
    Question[] m_questions;

    void reset() {
        m_is_test_active = false;
        m_correct_ans_cnt = 0;
        m_questions = [];
    }

    void set_questions(RandomSample!(Question[], void) questions) {
        m_questions.length = questions.length;
        m_total_q_cnt = questions.length;

        uint i = 0;
        foreach (e; questions) {
            m_questions[i++] = e;
        }
    }

    Question pop_question() {
        Question resp;

        if (!m_questions.length) {
            throw new Exception("Try to pop question from empty queue");
        }

        resp = m_questions[0];
        if (m_questions.length > 1) {
            m_questions = m_questions[1 .. $];
        } else {
            m_questions = [];
            m_is_test_active = false;
        }
        return resp;
    }

    @property string result() {
        string res = "The test ";
        res ~= m_is_test_active ? "is in progress" : "was completed";
        res ~= ". Score: " ~ to!string(m_correct_ans_cnt) ~ " correct answers from " ~ to!string(m_total_q_cnt);
        return res;
    }
    @property Question cur_question() {
        return m_questions[0];
    }
}

class QuizletRouter : Router {
    Questions m_questions;
    UserContext[ulong] m_ctx_arr;
    Config m_cfg;
    ulong m_cur_ctx;
    uint m_question_cnt = 10;

    void reset() {
        m_ctx_arr[m_cur_ctx].reset();
    }

    string start_handler(const string[] argv) {
        return "Nice to see you!\n\n" ~ help_handler(argv);
    }

    string help_handler(const string[] argv) {
        return HELP_MESSAGE;
    }

    string test_handler(const string[] argv) {
        size_t portion_cnt = (m_questions.questions.length > m_question_cnt) ? m_question_cnt : m_questions.questions.length;
        auto selected_q = randomSample(m_questions.questions, portion_cnt);

        m_ctx_arr[m_cur_ctx].reset();
        m_ctx_arr[m_cur_ctx].set_questions(selected_q);
        m_ctx_arr[m_cur_ctx].m_is_test_active = true;

        return m_ctx_arr[m_cur_ctx].cur_question.toString;
    }

    string ans_handler(const string[] argv) {
        string resp;
        int answer = to!int(argv[1]);

        if (m_ctx_arr[m_cur_ctx].m_is_test_active) {
            if (m_ctx_arr[m_cur_ctx].cur_question.answer == answer) {
                m_ctx_arr[m_cur_ctx].m_correct_ans_cnt++;
            }

            m_ctx_arr[m_cur_ctx].pop_question();

            if (m_ctx_arr[m_cur_ctx].m_questions.length > 0) {
                resp = m_ctx_arr[m_cur_ctx].cur_question.toString;
            } else {
                resp = m_ctx_arr[m_cur_ctx].result;
            }
        } else {
            resp = "Please start the quiz by typing /test command";
        }

        return resp;
    }

    string reset_handler(const string[] argv) {
        reset();
        return "Quiz was aborted";
    }

    string kill_handler(const string[] argv) {
        string res;
        if (canFind(m_cfg.admins, m_cur_ctx)) {
            res = "Shutdown the server";
        } else {
            res = "You do not have a permission to do that";
        }
        return res;
    }

public:
    this(Config cfg) {
        this.m_cfg = cfg;
    }

    override string route(const string req) {
        string[] words = req.split;
        string res;

        switch (words[0]) {
            case "start": {
                res = start_handler(words);
                break;
            }
            case "help": {
                res = help_handler(words);
                break;
            }
            case "test": {
                res = test_handler(words);
                break;
            }
            case "ans": {
                res = ans_handler(words);
                break;
            }
            case "reset": {
                res = reset_handler(words);
                break;
            }
            case "kill": {
                res = kill_handler(words);
                break;
            }
            default: {
                res = "Unknown command: " ~ req ~ "\nPlease try /help";
                break;
            }
        }

        return res;
    }

    void switch_context(ulong id) {
        m_cur_ctx = id;
        if (id !in m_ctx_arr) {
            m_ctx_arr[m_cur_ctx] = UserContext();
        }
    }

    @property Questions questions() { return m_questions; }
    @property Questions questions(Questions q) { return m_questions = q; }

    @property bool is_test() { return m_ctx_arr[m_cur_ctx].m_is_test_active; }
    @property size_t variants_cnt() { return m_ctx_arr[m_cur_ctx].cur_question.variants.length; }
}